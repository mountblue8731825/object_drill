const invert = require("../invert");
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" }; // use this object to test your functions

console.log(invert(testObject));
