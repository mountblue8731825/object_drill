const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" }; // use this object to test your functions

function keys(obj) {
  const keyarr = [];
  for (var key in obj) {
    keyarr.push(key);
  }
  return keyarr;
}

module.exports = keys;
