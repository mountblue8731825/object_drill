const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" }; // use this object to test your functions

function pairs(obj) {
  const pairarr = [];
  for (var key in obj) {
    pairarr.push([key, obj[key]]);
  }
  return pairarr;
}

module.exports = pairs;
