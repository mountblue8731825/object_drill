const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" }; // use this object to test your functions

function values(obj) {
  let valuearr = [];

  for (let key in obj) {
    valuearr.push(obj[key]);
  }
  return valuearr;
}

module.exports = values;
