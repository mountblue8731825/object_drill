const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" }; // use this object to test your functions

function mapObject(obj, cb) {
  var res = [];

  for (var key in obj) {
    res.push(cb(obj[key], key));
  }

  return res;
}

function cb(value, key) {
  return value + "oo";
}

module.exports = { mapObject, cb };
